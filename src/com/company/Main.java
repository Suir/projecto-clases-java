package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        Scanner input = new Scanner (System.in);

        float radio = darRadio(input);

        Circunferencia nueva = new Circunferencia(radio);

        darColor(nueva,input);

        System.out.println(nueva);
    }

    /**
     * Metodo para establecer color
     */
    /*En esta funcion uso por parametros: El objeto nueva de la clase Circunferencia y el objeto input de la clase
    Scanner. En la otra no uso el objeto porque el parámetro que devuelve (notese que la primera función es
    void y la segunda es normal) es necesario para el constructor del propio metodo.
    */
    private static void darColor (Circunferencia nueva,Scanner input){
        System.out.println("Dele color a la figura");

        String color = input.nextLine();

        nueva.setColor(color);

    }

    /**
     *Metodo para establecer radio
     */
    private static float darRadio (Scanner input){

        System.out.println("Introduce un radio");
        float radio = input.nextInt();

        //Limpiando Scanner
        input.nextLine();

        return radio;

    }
}


