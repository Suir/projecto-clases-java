package com.company;

public class Circunferencia extends Figura {
    private float radio;

    private float diametro;

    private float area;


   Circunferencia (float radio) {
       System.out.println("Estableciendo Radio");
       this.radio= radio;
       System.out.println("Calculando Diámetro");
       this.diametro= radio*2;
       System.out.println("Calculando área");
       this.area = 3.14159265f*radio*radio;

   }
    public float getRadio(){
       return this.radio;
    }
    public float getDiametro(){
       return this.diametro;
    }
    public float getArea() {
        return this.area;
    }

    @Override
    public String toString() {
        return "Es una circunferencia de color " + getColor()+
                ", radio " + radio + " cm"+
                ", diametro " + diametro + "cm"+
                ", area " + area + " cm^2";

    }
}
